#include "dialog.h"
#include "ui_dialog.h"
#include "thread.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::ShowTime(int value)
{

    ui->label->setText(QString::number(value));

}




