#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include <QtCore>





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Wybierz rodzaj czasu jaki chcesz odmierzyć");
    ui->comboBox->addItem("czas od rozpoczecia odliczania");
    ui->comboBox->addItem("czas od ostatniego otworzenia okna");
    mThread = new Thread(this);
    mDialog = new Dialog(this);
    connect(mThread,SIGNAL(TimeChanged(int)),this,SLOT(TimeRecieved(int)));
    connect(this,SIGNAL(TimeForWindow(int)),mDialog,SLOT(ShowTime(int)));



}

MainWindow::~MainWindow()
{
    delete ui;
}
// przycisk powoduje wystartowanie timera
void MainWindow::on_pushButton_clicked()
{

    mThread->start();

}
//przycisk powoduje wywolanie modalnego okna z pożądanym czasem
void MainWindow::on_pushButton_2_clicked()
{

    mThread->wcount = mThread->count;


    mDialog->setModal(true);
    mDialog->exec();
}
// ustawia czas na label
void MainWindow::TimeRecieved(int value)
{

    ui->label->setText(QString::number(value));

}


//sprawdza, która opcja została wybrana
void MainWindow::on_comboBox_activated(const QString &arg1)
{




    if(arg1=="czas od rozpoczecia odliczania")
    {

        emit TimeForWindow(mThread->count/10 );



    }else
        if(arg1=="czas od ostatniego otworzenia okna")
        {

            emit TimeForWindow((mThread->count - mThread->wcount)/10);


        }else
            if(arg1=="Wybierz rodzaj czasu jaki chcesz odmierzyć")
            {
                emit TimeForWindow(0);

            }

}


