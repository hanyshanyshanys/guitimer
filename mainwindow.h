#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "thread.h"
#include "dialog.h"



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Thread *mThread;
    Dialog *mDialog;


private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();


    void on_comboBox_activated(const QString &arg1);

signals:
    void TimeForWindow(int);


public slots:
    void TimeRecieved(int);





private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
