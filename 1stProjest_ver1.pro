#-------------------------------------------------
#
# Project created by QtCreator 2015-11-25T20:13:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 1stProjest_ver1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dialog.cpp \
    thread.cpp

HEADERS  += mainwindow.h \
    dialog.h \
    thread.h

FORMS    += mainwindow.ui \
    dialog.ui
